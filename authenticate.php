<?php
session_start();
$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'phploginsystem';
$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
if ( mysqli_connect_errno() ) {
	exit('Failed to connect to MySQL: ' . mysqli_connect_error());
}

// check if the data from the login form has been submitted, isset() will check if the data exists.
if ( !isset($_POST['username'], $_POST['password']) ) {
	// if the data in the request doesnt exist show this error
	exit('Please fill both the username and password fields!');
}

// preparing the SQL statement will help prevent SQL injection.
if ($sqlPrepare = $con->prepare('SELECT id, user_password FROM user_accounts WHERE username = ?')) {
	$sqlPrepare->bind_param('s', $_POST['username']);
	$sqlPrepare->execute();
	// Store the result so we can check if the user account exists in the database.
	$sqlPrepare->store_result();


	// $sqlPrepare->close();
}
print_r($sqlPrepare);
if ($sqlPrepare->num_rows > 0) {
	$sqlPrepare->bind_result($id, $password);
	$sqlPrepare->fetch();
	if (password_verify($_POST['password'], $password)) {
		session_regenerate_id();
		$_SESSION['loggedin'] = TRUE;
		header('Location: home.php');
		$_SESSION['id'] = $id;
		echo 'Welcome ' . $_SESSION['name'] . '!';
	} else {
		// Incorrect password
		echo 'Incorrect username and/or password!';
	}
} else {
	// Incorrect username
	echo 'Incorrect username and/or password!';
}
?>