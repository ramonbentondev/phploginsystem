<?php
session_start();
// If the user is not logged in then redirect to the login page...
if (!isset($_SESSION['loggedin'])) {
	header('Location: index.html');
	exit;
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Home Page</title>
		<link href="style.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
	</head>
	<body class="loggedin">
		<?php include 'navigation.html' ?>
		<div class="content">
			<h2>Home Page</h2>
			<p>Welcome back!</p>
		</div>
	</body>
</html>