Welcome to the PHP LOGIN SYSTEM READ ME!


Purpose:

This repository is an example of a simple php login and user registration system.

Setup:

Ubuntu version 20.0.4
Php 7.4
CSS
HTML
XAMPP
MySQL
PhpMyadmin

to run the project i have used Xampp becuase it is cross platform and very simple and easy to setup

install xampp - if on linux clone this repository to /opt/lamp/htdocs/ - to be consistent with my setup

if on windows install xamp and follow the instructions and clone this repository to any reasonable location such as Desktop or a specific code folder you use for development.

note i used phpmyadmin for the database interface but you are welcome to use any interface you see fit.



side note: i had previously included some activation logic that would mean users would be sent an email to generate an activation code to login, but getting you to configure the mail server for this small taks seemed a bit long winded!